from recomb_model import *
import random as random

final_time = 50000
population_sizes = [500, 200, 1000]
chrsm_lengths = [10000, 5000, 15000]
segment_lengths = [100, 20, 500]


for popsize in population_sizes:
    chrsm_len = chrsm_lengths[0]
    seg_len = segment_lengths[0]
    pop = Population(popsize,final_time, chrsm_len)
    while pop.T < final_time:
        if (pop.T%500)==0:
            print("current forward time", pop.T)
        pop.forward_step()

    for indiv_id in range(10): # We take 10 random individuals to start our reconstructions
        start_seg = random.randint(0, chrsm_len - seg_len)
        lin = Lineage(indiv_id, random.randint(0,1), start_seg, start_seg+seg_len, pop)
        while (lin.back_time<final_time-1 and
            ((not lin.has_separated)
             or (lin.has_separated and len(lin.lineage[lin.back_time])>1))):
            lin.backward_step()
            lin.check_fused_segments()
        filename = "pop_size-"+str(popsize)+"-chrsm_len-"+str(chrsm_len)+"-seg_len-"+str(seg_len)+"-rep-"+str(indiv_id)+".json"
        lin.write_data(filename)


for chrsm_len in chrsm_lengths:
    popsize = population_sizes[0]
    seg_len = segment_lengths[0]
    pop = Population(popsize,final_time, chrsm_len)
    while pop.T < final_time:
        pop.forward_step()

    for indiv_id in range(10): # We take 10 random individuals to start our reconstructions
        start_seg = random.randint(0, chrsm_len - seg_len)
        lin = Lineage(indiv_id, random.randint(0,1), start_seg, start_seg+seg_len, pop)
        while (lin.back_time<final_time-1 and
            ((not lin.has_separated)
             or (lin.has_separated and len(lin.lineage[lin.back_time])>1))):
            lin.backward_step()
            lin.check_fused_segments()
        filename = "pop_size-"+str(popsize)+"-chrsm_len-"+str(chrsm_len)+"-seg_len-"+str(seg_len)+"-rep-"+str(indiv_id)+".json"
        lin.write_data(filename)



for seg_len in segment_lengths:
    chrsm_len = chrsm_lengths[0]
    popsize = population_sizes[0]
    pop = Population(popsize,final_time, chrsm_len)
    while pop.T < final_time:
        pop.forward_step()

    for indiv_id in range(10): # We take 10 random individuals to start our reconstructions
        start_seg = random.randint(0, chrsm_len - seg_len)
        lin = Lineage(indiv_id, random.randint(0,1), start_seg, start_seg+seg_len, pop)
        while (lin.back_time<final_time-1 and
            ((not lin.has_separated)
             or (lin.has_separated and len(lin.lineage[lin.back_time])>1))):
            lin.backward_step()
            lin.check_fused_segments()
        filename = "pop_size-"+str(popsize)+"-chrsm_len-"+str(chrsm_len)+"-seg_len-"+str(seg_len)+"-rep-"+str(indiv_id)+".json"
        lin.write_data(filename)



